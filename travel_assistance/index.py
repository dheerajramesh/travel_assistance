# index.py
import sys, os
sys.path.append('/home/ubuntu/travel_assistance/env/lib/python3.6/site-packages/')

import google.protobuf  as pf
import site
import importlib
import pkg_resources
site.addsitedir('./tmp/lib/python3.6/site-packages')
importlib.reload(pkg_resources)
pkg_resources.get_distribution('google-api-core')

#from OpenSSL import SSL
#context = SSL.Context(SSL.PROTOCOL_SSLv23)
#context.use_privatekey_file('/home/dheeraj/travel_assistance/server.key')
#context.use_certificate_file('/home/dheeraj/travel_assistance/server.crt')

from flask import Flask, request, jsonify, render_template, make_response, redirect
import dialogflow
import requests
import json
import pusher

UPLOAD_FOLDER = 'static'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

#@app.before_request
#def force_https():
#    if request.endpoint in app.view_functions and not request.is_secure:
#        return redirect(request.url.replace('http://', 'https://'))

@app.route('/')
def index():
    full_filename = os.path.join(app.config['UPLOAD_FOLDER'], 'user.png')
    print(full_filename)
    return render_template('index.html')

# run Flask app
if __name__ == "__main__":
   app.run(host='0.0.0.0', ssl_context=('/home/ubuntu/travel_assistance/server.crt','/home/ubuntu/travel_assistance/server.key'))
#    app.run(host='0.0.0.0', ssl_context=context)

	
def detect_intent_texts(project_id, session_id, text, language_code):
    session_client = dialogflow.SessionsClient()
    session = session_client.session_path(project_id, session_id)

    if text:
        text_input = dialogflow.types.TextInput(
            text=text, language_code=language_code)
        query_input = dialogflow.types.QueryInput(text=text_input)
        response = session_client.detect_intent(
            session=session, query_input=query_input)
#    print('Query text: {}'.format(response.query_result.query_text))
#    print('Detected intent: {} (confidence: {})\n'.format(
#	response.query_result.intent.display_name,
#        response.query_result.intent_detection_confidence))
#    print('Fulfillment text: {}\n'.format(
#        response.query_result.fulfillment_text))
    if 'Done' in text:
        print(response.query_result.fulfillment_messages.payload.simple_responses.payload)
        return response.query_result.fulfillment_messages.payload.simple_responses.payload
    return response.query_result.fulfillment_text
    
@app.route('/webhook', methods=['POST'])
def webhook():
    speech = "Hello there, this reply is from the webhook !! "
    string = "You are awesome !!"
    Message ="this is the message"

    my_result =  {

    "fulfillmentText": string,
     "source": string
    }

    res = json.dumps(my_result, indent=4)
    return res

def webhook1():
    res = request.get_json(silent=True)
    print('webhook data:')
    print(res)
    """
    if data['queryResult']['queryText'] == 'yes':
        reply = {
            "fulfillmentText": "Ok. Tickets booked successfully.",
        }
        return jsonify(reply)

    elif data['queryResult']['queryText'] == 'no':
        reply = {
            "fulfillmentText": "Ok. Booking cancelled.",
        }
	"""
    r = make_response(res)
    r.headers['Content-Type'] = 'application/json'
    return jsonify(r)


@app.route('/send_message', methods=['POST'])
def send_message():
    message = request.form['message']
    project_id = os.getenv('DIALOGFLOW_PROJECT_ID')
    if message == 'yesterday':
        fulfillment_text = 'Not possible for yesterday'
    else:
    	#fulfillment_text = webhook()
    	fulfillment_text = detect_intent_texts(project_id, "unique", message, 'en')    
    response_text = { "message":  fulfillment_text }
    return jsonify(response_text)
